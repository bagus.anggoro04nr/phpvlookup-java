package vlookup.files;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import vlookup.config.dbinterbase;
import vlookup.funcs.AllFunc;
import vlookup.funcs.dbFunc;

import java.io.*;
import java.sql.SQLException;

public class readFile {

    private String[] pathData;
    private int a, b;
    private dbinterbase rf;
    private String[] cols;
    private String rows;
    private XSSFWorkbook workbook;
    private XSSFSheet sheet;
    private File file;
    private FileInputStream inputStream;
    private String[] cog;
    private BufferedInputStream bis = null;
    private DataInputStream dis = null;
    private boolean dropTbl;


    public readFile(String[] dataPath, String col, String row)  {
        pathData = dataPath;
        cog = col.split(",");
        cols = new AllFunc().letterToInt(cog);
        rows = row;
        dropTbl = true;
    }

    public boolean dropTables() throws SQLException, ClassNotFoundException {
        rf = new dbinterbase();
        dropTbl = rf.dropTable();
        return dropTbl;
    }

    public void readFiles() throws IOException, SQLException, ClassNotFoundException {

        a=0;
        while (a < pathData.length) {

            file = new File("" + pathData[a]);
            inputStream = new FileInputStream(file);
            bis = new BufferedInputStream(inputStream);
            dis = new DataInputStream(bis);
            workbook = new XSSFWorkbook(dis);

            sheet = workbook.getSheetAt(0);
            dbFunc ct = new dbFunc();
//            System.out.println(rows + " rower ");
            ct.createTable(sheet, a, rows, cols);
//            System.out.println(a + " nilai A "+ pathData[a]);
//            System.out.println("Create Table Sukses ");
            inputStream.close();
            bis.close();
            dis.close();
            a++;
        }
//        System.out.println("Create All Table Sukses");
        b=0;
        while (b < pathData.length){
            System.out.println(pathData[b]);
            file = new File("" + pathData[b]);
            inputStream = new FileInputStream(file);
            bis = new BufferedInputStream(inputStream);
            dis = new DataInputStream(bis);
            workbook = new XSSFWorkbook(dis);

            sheet = workbook.getSheetAt(0);
            dbFunc it = new dbFunc();
            it.insertTable(sheet, b, rows, cols);
            inputStream.close();
            bis.close();
            dis.close();
            b++;
//            System.out.println("Insert Table Sukses");
        }
//        System.out.println("Insert All Table Sukses");
    }

    public JSONArray hasilCompare(String col) throws ClassNotFoundException, SQLException {
        cog = col.split(",");
        return new dbinterbase().queryCompare(cog);
    }

    public JSONArray hasilCompareNull(String col) throws ClassNotFoundException, SQLException {
        cog = col.split(",");
        return new dbinterbase().queryCompareNull(cog);
    }

}
