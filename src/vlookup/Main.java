package vlookup;

import org.json.JSONArray;
import vlookup.files.readFile;

import java.io.IOException;
import java.sql.SQLException;

public class Main {

    public static String main(String[] args) throws ClassNotFoundException, SQLException, IOException, NullPointerException {

        String[] pathData = args[0].split(",");
        String col = args[1];

//        System.out.print(Arrays.toString(pathData));

        readFile run = new readFile(pathData, col, args[2]);
        if(!run.dropTables())run.readFiles();
        else{run.dropTables();run.readFiles();}
        JSONArray resultHasil = run.hasilCompare(col);
//        JSONArray resultHasilNull = run.hasilCompareNull(col);
//        System.out.println(resultHasil);
//        System.out.println("\r\n");
//        System.out.println(resultHasilNull);
//        System.out.println("\r\n");
//        System.out.println("Create DB or Table Sukses;");
        return String.valueOf(resultHasil);
    }

}
