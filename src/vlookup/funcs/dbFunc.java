package vlookup.funcs;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import vlookup.config.dbinterbase;

import java.sql.SQLException;
import java.util.Arrays;

public class dbFunc {

    private int a;
    private int r;
    private int c;
    private int rfs;
    private int rj;
    private dbinterbase rf;
    private int highestRows;
    private int strlen;
    private String query, x, x1, select, selectfull;
    private String dataOnRow;
    private XSSFCell cellExcel;

    public dbFunc(){

    }

    public void createTable(XSSFSheet dataExcel, int numy, String rowse, String[] cols) throws SQLException, ClassNotFoundException {

        highestRows = dataExcel.getLastRowNum();
        String[] colds = Arrays.toString(cols).split(",");
//        System.out.println(Arrays.toString(colds));
//        System.out.println(colds.length);
        if(numy > 0){
            rfs=0;
            a = 0;
        }else a =0;
        System.out.println(rowse + " rowse ");
        for(c=a; c < colds.length; c++){

            strlen = new ArrayFuncs().firstValArray(rowse, highestRows, dataExcel, c, colds);

//            System.out.println(c);
            rf = new dbinterbase();
            rf.createTables(numy, c, strlen, rfs);
//            rj = rf.createTables(numy, c, strlen, rfs);
//            if (rj == 1 && c == (colds.length - 1)) {
//                rfs = 1;
//            } else if (rj == 1) {
//                if (numy >= 0 && c == colds.length) rfs = 1;
//                else if (c == (colds.length - 1)) rfs = 1;
//
//            }
            strlen=0;
        }
    }

    public void insertTable(XSSFSheet dataExcels, int nums, String rows, String[] cols) throws ClassNotFoundException, SQLException {
        highestRows = dataExcels.getLastRowNum();

        String[] colds = Arrays.toString(cols).split(",");

        if (nums == 0) query = "INSERT INTO fl_data1 VALUES";
        else query = "INSERT INTO fl_data2 VALUES";
        dataOnRow="";
//        System.out.println("Insert Table");
        for(r = Integer.parseInt(rows); r <= highestRows; r++){

            dataOnRow += query + "(";
            for(c=0; c < colds.length; c++){

                int gd = Integer.parseInt(colds[c].replace("[", "").replace("]", ""));

                if(c > 0) dataOnRow += ", ";
                cellExcel = dataExcels.getRow(r).getCell(gd-1);
                switch(cellExcel.getCellType()){
                    case _NONE:
                        break;
                    case NUMERIC:
                        dataOnRow += "'" + String.valueOf(cellExcel.getNumericCellValue()) + "'";
                        break;
                    case STRING:
                        dataOnRow += "'" + cellExcel.getStringCellValue().replace("\"", "").
                                replace("\r\n", "") + "'";
                        break;
                    case FORMULA:
                        break;
                    case BLANK:
                        break;
                    case BOOLEAN:
                        break;
                    case ERROR:
                        break;
                }

            }
            dataOnRow += "); ";

            if(!dataOnRow.equals("")) {
                if (nums == 0) {
                    rf = new dbinterbase();
                    rf.insertToTable(dataOnRow);
                    dataOnRow = "";
                } else {
                    rf = new dbinterbase();
                    rf.insertToTable(dataOnRow);
                    dataOnRow = "";
                }
            }
        }

    }

    public String selectColumnTable(int data, String[] col){

        if(data == 1) x = "a.dt1";
        x1 = "b.dt2";

        select ="";
        selectfull ="";

        for(int b = 0; b<col.length; b++) {
            if (b > 0) select = " and ";
            select += "( " + x + String.valueOf(b) + " IN (" ;

            for(a = 0; a < col.length; a++) {

                if (a > 0) select += ", ";

                select +=  x1 + String.valueOf(a) ;


            }
            select += " )) ";


            selectfull += select;
        }

        return selectfull;
    }

}
