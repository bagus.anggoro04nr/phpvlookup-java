package vlookup.funcs;

import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;

public class ArrayFuncs {

    private int strlen;
    private int lens;
    private XSSFCell cellExcel, cellExcel1;
    private int maxs;
    private int rowData;

    public ArrayFuncs(){}

    public int firstValArray(String rowsy, int hightRows, XSSFSheet dataExcel, int c, String[] colds){
        System.out.println(rowsy + " rowsy ");
        int gd = Integer.parseInt(colds[c].replace("[", "").replace("]", ""));
        lens = maxValArray(rowsy, hightRows, dataExcel, gd);
        return lens;

    }

    private int maxValArray(String rowsyr, int hightRows, XSSFSheet dataExcel, int gds){
        System.out.println(rowsyr + " rowsyr ");
        cellExcel1 = dataExcel.getRow(Integer.parseInt(rowsyr)).getCell(gds-1);
        if(cellExcel1.getCellType()==CellType.STRING)
            maxs = cellExcel1.getStringCellValue().length();
        else if(cellExcel1.getCellType()== CellType.NUMERIC)
            maxs = String.valueOf(cellExcel1.getNumericCellValue()).length();

        int r;
        for ( r = Integer.parseInt(rowsyr); r < hightRows; r++) {

            cellExcel = dataExcel.getRow(r).getCell(gds-1);
            if(cellExcel.getCellType()==CellType.STRING)
                rowData = cellExcel.getStringCellValue().length();
            else if(cellExcel.getCellType()== CellType.NUMERIC)
                rowData = String.valueOf(cellExcel.getNumericCellValue()).length();


            if (rowData >= maxs) {
                strlen = rowData;
            }
            maxs = strlen;
        }
        return strlen;
    }
}
