package vlookup.config;

import org.firebirdsql.gds.impl.GDSType;
import org.firebirdsql.management.FBManager;
import org.json.JSONArray;
import org.json.JSONObject;
import vlookup.funcs.dbFunc;

import java.io.File;
import java.sql.*;

public class dbinterbase {

    private String pass, dbname;
    private String pathDB, pathApp, paths;
    private Statement stmt;
    private Connection connection;
    private String query;
    private boolean cr;
    private int cr1;
    private ResultSet rs;
    private JSONArray hasil;
    private JSONObject obj;
    private String field;
    private ResultSet drp2;
    private ResultSet drp1;
    private ResultSet drp3;
    private boolean drpT;
    private ResultSetMetaData rsMetaData;
    private String colName, userName;
    private int numColumns;
    private File file;
    private FBManager manager;


    public dbinterbase() throws SQLException, ClassNotFoundException {
        Class.forName("org.firebirdsql.jdbc.FBDriver");
        dbname = "vlookup.fdb";
        query = "";
        pass = "bagus4nr";
        userName = "SYSDBA";
        pathApp = System.getProperty("user.dir");
        paths = pathApp + "\\db\\" + dbname.toUpperCase();
        pathDB = "jdbc:firebirdsql://localhost:3050/" + paths ;



        file = new File(paths);

        if(!file.getParentFile().exists()){
            file.getParentFile().mkdirs();
        }

        if(!file.exists()){
            try {
                createDB();
                connection = DriverManager.getConnection(pathDB, userName, pass);

                stmt = connection.createStatement(
                        ResultSet.TYPE_SCROLL_INSENSITIVE,
                        ResultSet.CONCUR_READ_ONLY
                );

                if (stmt != null) {
                    stmt.close();
                    stmt = connection.createStatement(
                            ResultSet.TYPE_SCROLL_INSENSITIVE,
                            ResultSet.CONCUR_READ_ONLY
                    );
                }else stmt = connection.createStatement(
                        ResultSet.TYPE_SCROLL_INSENSITIVE,
                        ResultSet.CONCUR_READ_ONLY
                );

                if (drp1 != null) {
                    drp1.close();
                }

                if (drp2 != null) {
                    drp2.close();
                }

                if (drp3 != null) {
                    drp3.close();
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        }else {

            connection = DriverManager.getConnection(pathDB, userName, pass);

            stmt = connection.createStatement(
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY
            );

            if (stmt != null) {
                stmt.close();
                stmt = connection.createStatement(
                        ResultSet.TYPE_SCROLL_INSENSITIVE,
                        ResultSet.CONCUR_READ_ONLY
                );
            }else stmt = connection.createStatement(
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY
            );

        }
        if (drp1 != null) {
            drp1.close();
        }

        if (drp2 != null) {
            drp2.close();
        }

        if (drp3 != null) {
            drp3.close();
        }
    }

    private void createDB() throws Exception {

        manager = new FBManager(GDSType.getType("PURE_JAVA"));
        manager.start();
        manager.createDatabase(paths, userName, pass);
        manager.stop();

    }

    public boolean dropTable() throws SQLException {
        drpT = false;
        query = "select rdb$relation_name as NameTable\n" +
                "from rdb$relations\n" +
                "where rdb$view_blr is null\n" +
                "and (rdb$system_flag is null or rdb$system_flag = 0);";
        drp3 = stmt.executeQuery(query.toUpperCase());
        assert drp3 != null;
        while (drp3.next()) {
            drpT = !(drp3.getString("NameTable") == null | drp3.getString("NameTable").equals(""));
        }
        drp3.close();
        query ="";
        if (drpT){
            query = "DROP TABLE fl_data1";
            stmt.execute(query.toUpperCase());
            query = "";
            query = "DROP TABLE fl_data2";
            stmt.execute(query.toUpperCase());
            query = "";
            drpT = true;
        }
//        query = "SELECT 1 as RS FROM RDB$RELATIONS\n" +
//                "  WHERE RDB$RELATION_NAME = 'FL_DATA1';";
//        drp1 = stmt.executeQuery(query.toUpperCase());
//        assert drp1 != null;
//         while (drp1.next()) {
//                if (drp1.getString("RS") == null | drp1.getString("RS").equals("")) drpT = false;
//                else {
//                    query = "DROP TABLE fl_data1";
//                    stmt.execute(query.toUpperCase());
//                    query = "";
//                    drpT = true;
//                }
//          }
//
//        query = "";
//        drp1.close();
//
//        query = "SELECT 1 as RS1 FROM RDB$RELATIONS\n" +
//                "  WHERE RDB$RELATION_NAME = 'FL_DATA2'";
//
//        drp2 = stmt.executeQuery(query.toUpperCase());
//        assert drp2 != null;
//        while (drp2.next()) {
//                if (drp2.getString("RS1") == null | drp2.getString("RS").equals("")) drpT = false;
//                else {
//                    query = "DROP TABLE fl_data2";
//                    stmt.execute(query.toUpperCase());
//                    query = "";
//                    drpT = true;
//                }
//        }
//
//        drp2.close();
//        query = "";
        stmt.close();
        System.out.println(drpT);
        return drpT;
    }

    public void createTables(int nums, int col, int len, int crs) throws SQLException {

        if(nums == 0) {
            if (col == 0) {
                System.out.println("Create Table");
                query += "CREATE TABLE fl_data1(";
                query += "dt1" + col + " varchar("+ len +") NOT NULL";
                query += ");";
                System.out.println(query.toUpperCase());
                stmt.execute(query.toUpperCase());
                query = "";

                query = "ALTER CHARACTER SET UTF8 SET DEFAULT COLLATION UTF8;";
                stmt.execute(query.toUpperCase());
                query = "";

            }else{
                System.out.println("Alter Table");
                query += "ALTER TABLE fl_data1 ADD";
                query += " dt1" + col + " varchar("+ len +") NOT NULL;";
                System.out.println(query.toUpperCase());
                stmt.execute(query.toUpperCase());
                query = "";

                query = "ALTER CHARACTER SET UTF8 SET DEFAULT COLLATION UTF8;";
                stmt.execute(query.toUpperCase());
                query = "";

            }

        }else{

            if (col == 0) {
                query += "CREATE TABLE fl_data2 (";
                query += "dt2" + col + " varchar("+ len +") NOT NULL";
                query += ");";
                System.out.println(query.toUpperCase());
                stmt.execute(query.toUpperCase());
                query = "";

                query = "ALTER CHARACTER SET UTF8 SET DEFAULT COLLATION UTF8;";
                stmt.execute(query.toUpperCase());
                query = "";

            } else {
                query += "ALTER TABLE fl_data2 ADD";
                query += " dt2" + col + " varchar("+ len +") NOT NULL;";
                System.out.println(query.toUpperCase());
                stmt.execute(query.toUpperCase());
                query = "";

                query = "ALTER CHARACTER SET UTF8 SET DEFAULT COLLATION UTF8;";
                stmt.execute(query.toUpperCase());
                query = "";

            }

        }
        stmt.close();

    }

    public void insertToTable(String data) throws SQLException {


        query = "";
        query = data;
//        System.out.println(query.toUpperCase());
//        System.out.println("insert");
        stmt.executeBatch();
        cr = stmt.execute(query.toUpperCase());
        query = "";
        stmt.close();
    }

    public JSONArray queryCompare(String[] col) throws SQLException {

        field = new dbFunc().selectColumnTable(1, col);
        query = "SELECT * FROM fl_data1 AS a INNER JOIN fl_data2 AS b ON " + field + " ;";
//        System.out.println(query.toUpperCase());
        rs = stmt.executeQuery(query.toUpperCase());
        assert rs != null;

        rsMetaData = rs.getMetaData ();
        hasil = new JSONArray();
        numColumns = rsMetaData.getColumnCount();
         while (rs.next()) {
             obj = new JSONObject();
             for (int i = 1; i <= (numColumns); i++) {
                 colName = rsMetaData.getColumnLabel(i);
                 if (colName != null) {
                     obj.put(colName, rs.getObject(colName));
                 }
                colName="";
             }
             hasil.put(obj);
         }
        rsMetaData = null;
        rs.close();
        query = "";
        stmt.close();
//        System.out.println(Arrays.toString(hasil));
        return hasil;
    }

    public JSONArray queryCompareNull(String[]  col) throws SQLException {

        field = new dbFunc().selectColumnTable(1, col);
        query = "SELECT * FROM fl_data1 AS a INNER JOIN fl_data2 AS b ON " + field + " WHERE b.dt20 IS NULL;";
//        System.out.println(query.toUpperCase());
        rs = stmt.executeQuery(query.toUpperCase());
        assert rs != null;
        rsMetaData = rs.getMetaData ();
        hasil = new JSONArray();
        numColumns = rsMetaData.getColumnCount();
        while (rs.next()) {
            obj = new JSONObject();
            for (int i = 1; i <= (numColumns); i++) {
                colName = rsMetaData.getColumnLabel(i);
                if (colName != null) {
                    obj.put(colName, rs.getObject(colName));
                }
                colName="";
            }
            hasil.put(obj);
        }
        rsMetaData = null;
        rs.close();
        query = "";
        stmt.close();
//        System.out.println(Arrays.toString(hasil));
        return hasil;
    }
}
